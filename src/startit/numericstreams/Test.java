package startit.numericstreams;

import java.util.Arrays;
import java.util.List;

public class Test {

    public static void main(String[] args) {
        List<Item> items = Arrays.asList(

                new Item("towels", 300),
                new Item("sheets", 450)
        );
        int totalQuantity = items.stream()
                .mapToInt(i -> i.getQuantity())
                .sum();

        System.out.println("Total quantity = " + totalQuantity);
    }
}
